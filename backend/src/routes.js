const express = require('express');


// constante para rotas 
const routes = express.Router();


// importar o controller

const OngController = require('./controller/OngController')
const IncidentController = require('./controller/IncidentController')
const ProfileController = require('./controller/ProfileController')
const SessionsController = require('./controller/SessionController')



routes.get('/ongs', OngController.index);
routes.post('/sessions', SessionsController.create);

routes.post('/ongs', OngController.create);

routes.get('/profile', ProfileController.index);

routes.get('/incidents', IncidentController.index);
routes.post('/incidents', IncidentController.create);

routes.delete('/incidents/:id', IncidentController.delete);



// exporta as rotas para obter pelo index 
module.exports = routes;
