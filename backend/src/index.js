const express = require('express');

// recurso de segurança cors

const cors = require('cors');



// importar as rotas (./) indicando arquivo
const routes = require('./routes')

const app = express();

app.use(cors());

app.use(express.json());

app.use(routes);



app.listen(3333);